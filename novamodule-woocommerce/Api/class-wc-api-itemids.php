<?php
include_once("debugLogger.php");
class WC_API_Itemids extends WC_API_Resource {
	protected $base = '/itemids';
	public $logger;
	function __construct(WC_API_Server $server){
		$this->logger = new NovaLogger("novamodule-itemids.log");
		parent::__construct($server);
	}

	public function register_routes( $routes ) {
		$routes[ $this->base ] = array(
			array( array( $this, 'get_itemids' ), WC_API_Server::READABLE )
		);

		return $routes;
	}
	
	public function get_itemids() {
		$data = $this->getListOfIds();
		return $data;
	}
	public function getListOfIds() {
		global $wpdb;
		$finalResults = array();
		$posttable = $wpdb->prefix."posts";
		$postmetatable = $wpdb->prefix."postmeta";
		
		$results = $wpdb->get_results( "SELECT ID, b.meta_value as sku,c.meta_value as parent_sku, a.post_parent as parent_id from ".$posttable." a LEFT JOIN ".$postmetatable." b on a.id=b.post_id AND b.meta_key = '_sku'  LEFT JOIN ".$postmetatable." c on a.post_parent=c.post_id AND c.meta_key = '_sku' WHERE (a.post_type='product_variation' OR a.post_type='product') AND (b.meta_value IS NOT NULL OR c.meta_value IS NOT NULL) group by ID");
		$this->logger->debugLogger("====================");
        $this->logger->debugLogger("SELECT ID, b.meta_value as sku,c.meta_value as parent_sku, a.post_parent as parent_id from ".$posttable." a LEFT JOIN ".$postmetatable." b on a.id=b.post_id AND b.meta_key = '_sku'  LEFT JOIN ".$postmetatable." c on a.post_parent=c.post_id AND c.meta_key = '_sku' WHERE (a.post_type='product_variation' OR a.post_type='product') AND (b.meta_value IS NOT NULL OR c.meta_value IS NOT NULL) group by ID");
		$this->logger->debugLogger("====================");
		foreach($results as $key =>$value){
			if(!$value->sku){
				$value->sku = $value->parent_sku;
			}
			$finalResults[] = $value;
		}
		return $finalResults;
	}
}
