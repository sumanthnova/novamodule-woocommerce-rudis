<?php
/*
  Plugin Name: Novamodule Woocommerce
  Description: Novamodule plug-in helpful to integrate with IO. Woocommerce should be installed and active to use this plugin.
  Version: 1.1.3
  Author: Novamodule
  Author URI: http://www.novamodule.com/
 */
add_action('admin_init', 'nova_plugin_has_woocomerce_plugin');

//include_once( 'class-wc-api-custom.php' );
function nova_plugin_has_woocomerce_plugin() {
    if (is_admin() && current_user_can('activate_plugins') && !is_plugin_active('woocommerce/woocommerce.php')) {
        add_action('admin_notices', 'nova_plugin_notice');

        deactivate_plugins(plugin_basename(__FILE__));

        if (isset($_GET['activate'])) {
            unset($_GET['activate']);
        }
    }
}

function nova_plugin_notice() {
    ?><div class="error" style="background-color: red;"><p>Sorry, Novamodule Plugin requires the Woocommerce plugin to be installed and active.</p></div><?php
}

##### Custom Api ######
/* add_action( 'woocommerce_api_loaded', function(){
  include_once( 'class-wc-api-custom.php' );
  });
  add_filter( 'woocommerce_api_classes', function( $classes ){
  $classes[] = 'WC_API_Custom';
  return $classes;
  }); */
add_action('woocommerce_api_loaded', 'wpc_register_wp_api_endpoints');

function wpc_register_wp_api_endpoints() {
    include_once( 'Api/class-wc-api-itemids.php' );
    include_once( 'Api/class-wc-api-bulkfulfillment.php' );
    include_once( 'Api/class-wc-api-bulkorderidupdate.php' );
    add_filter('woocommerce_api_classes', 'filter_woocommerce_api_classes', 10, 1);
}

function filter_woocommerce_api_classes($array) {
    $cart = array(
        'WC_API_Itemids',
        'WC_API_Bulkfulfillment',
        'WC_API_Bulkorderidupdate'
    );
    return array_merge($array, $cart);
}

add_action( 'rest_api_init', 'register_api_hooks' );
function register_api_hooks() {
	include_once('Rest-W3-Api/class-wc-api-bulkproducts.php');
	include_once('Rest-W3-Api/class-wc-api-bulkinventory.php');
	$WC_REST_Bulkproducts_Controller = new WC_REST_Bulkproducts_Controller;
	$WC_REST_Bulkproducts_Controller->register_routes();
	
	$WC_REST_Bulkinventory_Controller = new WC_REST_Bulkinventory_Controller;
	$WC_REST_Bulkinventory_Controller->register_routes();
}


/* add_action('woocommerce_api_loaded', 'wpc_register_wp_api_endpoints');
  function wpc_register_wp_api_endpoints() {
  include_once( 'class-wc-api-custom.php' );
  add_filter('woocommerce_api_classes', 'filter_woocommerce_api_classes', 10, 1);
  }
  function filter_woocommerce_api_classes($array) {
  $cart = array(
  'WC_API_Resource',
  );
  return array_merge($array, $cart);
  }
 */
/* add_action( 'woocommerce_api_loaded', function(){
  include_once( 'class-wc-api-custom.php' );
  });
  add_filter( 'woocommerce_api_classes', function( $classes ){
  $classes[] = 'WC_API_Custom';
  return $classes;
  }); */
##### Custom Api End ######
#####Add additional filters for customers list to get Latest Modified ##########
add_filter('woocommerce_rest_customer_query', 'novamodule_woocommerce_rest_customer_query', 100, 2);

function novamodule_woocommerce_rest_customer_query($prepared_args, $request) {

    if (isset($_GET["type"]) && $_GET["type"] == "delta" && isset($_GET["io_last_run"]) && $_GET["io_last_run"] != "") {
        $io_last_run = filter_var($_GET["io_last_run"], FILTER_SANITIZE_NUMBER_INT);
        $lastrunDateTime = $io_last_run / 1000;
        if (isset($_GET["io_date_field"]) && $_GET["io_date_field"] == "modified") {
            if (isset($_GET["io_customer_timezone_diff"]) && $_GET["io_customer_timezone_diff"] != "") {
                $io_customer_timezone_diff = filter_var($_GET["io_customer_timezone_diff"], FILTER_SANITIZE_NUMBER_INT);

                $new_updated_date = date("Y-m-d H:i:s", strtotime($io_customer_timezone_diff, $lastrunDateTime));
                $lastrunDateTime = strtotime($new_updated_date);
            }
            $prepared_args["meta_query"][] = array('key' => 'last_update', 'value' => $lastrunDateTime, 'compare' => '>=');
        } else {
            $prepared_args['date_query'] = array("after" => date("Y-m-d H:i:s", $lastrunDateTime));
        }
        //debugLogger(json_encode($prepared_args));
        //debugLogger(json_encode($_GET));
    }

    return $prepared_args;
}

//after=2017-04-22T19:28:08
################################END#############################################
#####Add additional filters for Orders list to get Latest Modified ##########
add_filter('woocommerce_rest_shop_order_object_query', 'novamodule_woocommerce_rest_shop_order_object_query', 100, 2); //V3 Version
function novamodule_woocommerce_rest_shop_order_object_query($prepared_args, $request) {	 
	 
	 if (isset($_GET["meta_query"]) && is_array($_GET["meta_query"]) ) {
		 $meta_filters = $_GET["meta_query"];
		 foreach($meta_filters as $metaData) {
			 $prepared_args['meta_query'][] = $metaData;
		 }
		 
	 }
    //debugLogger(json_encode($prepared_args));
    if (isset($_GET["type"]) && $_GET["type"] == "delta" && isset($_GET["io_last_run"]) && $_GET["io_last_run"] != "") {
        $io_last_run = filter_var($_GET["io_last_run"], FILTER_SANITIZE_NUMBER_INT);

        $lastrunDateTime = $io_last_run / 1000;
        $prepared_args['date_query'] = array("after" => date("Y-m-d H:i:s", $lastrunDateTime));

        //debugLogger(json_encode($_GET));
    }
    if (isset($_GET["type"]) && $_GET["type"] != "delta" && isset($_GET["io_last_run"]) && $_GET["io_last_run"] != "") {
        $io_last_run = filter_var($_GET["io_last_run"], FILTER_SANITIZE_NUMBER_INT);

        $lastrunDateTime = $io_last_run; // / 1000;        
        $prepared_args['date_query'] = array("after" => date("Y-m-d H:i:s", $lastrunDateTime));

        //debugLogger(json_encode($_GET));
    }
    /* if($prepared_args["post_type"] == "shop_order" &&  isset($prepared_args["meta"]) && $prepared_args["meta"]== true && 
      isset($prepared_args["meta_key"]) && $prepared_args["meta_key"] == "nm_ns_pushed") {

      } */
    //debugLogger(json_encode($prepared_args));
    /* debugLogger(json_encode($prepared_args));
      $prepared_args['post_status'] = array("processing" , "processing");
      $prepared_args['status'] =  "processing,processing";
      debugLogger(json_encode($prepared_args)); */
    if (isset($_GET["custom_date"]) && isset($_GET["filter"]["updated_at_min"]) && $_GET["filter"]["updated_at_min"] != "") {

        $updated_at_min = filter_var($_GET["filter"]["updated_at_min"], FILTER_SANITIZE_NUMBER_INT);

        $updated_date = date("Y-m-d H:i:s", strtotime('-2 hours', $updated_at_min));
        if (isset($_GET["io_salesorder_timezone_diff"]) && $_GET["io_salesorder_timezone_diff"] != "") {

            $io_salesorder_timezone_diff = filter_var($_GET["io_salesorder_timezone_diff"], FILTER_SANITIZE_NUMBER_INT);
            $updated_date = date("Y-m-d H:i:s", strtotime($io_salesorder_timezone_diff, $updated_at_min));
        }
        $prepared_args['date_query'][] = array('column' => 'post_modified_gmt', 'after' => $updated_date, 'inclusive' => true);
    } 

    return $prepared_args;
	
}
add_filter('woocommerce_rest_shop_order_query', 'novamodule_woocommerce_rest_shop_order_query', 100, 2);

function novamodule_woocommerce_rest_shop_order_query($prepared_args, $request) {


    //debugLogger(json_encode($prepared_args));
    if (isset($_GET["type"]) && $_GET["type"] == "delta" && isset($_GET["io_last_run"]) && $_GET["io_last_run"] != "") {
        $io_last_run = filter_var($_GET["io_last_run"], FILTER_SANITIZE_NUMBER_INT);

        $lastrunDateTime = $io_last_run / 1000;
        $prepared_args['date_query'] = array("after" => date("Y-m-d H:i:s", $lastrunDateTime));

        //debugLogger(json_encode($_GET));
    }
    if (isset($_GET["type"]) && $_GET["type"] != "delta" && isset($_GET["io_last_run"]) && $_GET["io_last_run"] != "") {
        $io_last_run = filter_var($_GET["io_last_run"], FILTER_SANITIZE_NUMBER_INT);

        $lastrunDateTime = $io_last_run; // / 1000;        
        $prepared_args['date_query'] = array("after" => date("Y-m-d H:i:s", $lastrunDateTime));

        //debugLogger(json_encode($_GET));
    }
    /* if($prepared_args["post_type"] == "shop_order" &&  isset($prepared_args["meta"]) && $prepared_args["meta"]== true && 
      isset($prepared_args["meta_key"]) && $prepared_args["meta_key"] == "nm_ns_pushed") {

      } */
    //debugLogger(json_encode($prepared_args));
    /* debugLogger(json_encode($prepared_args));
      $prepared_args['post_status'] = array("processing" , "processing");
      $prepared_args['status'] =  "processing,processing";
      debugLogger(json_encode($prepared_args)); */
    if (isset($_GET["custom_date"]) && isset($_GET["filter"]["updated_at_min"]) && $_GET["filter"]["updated_at_min"] != "") {

        $updated_at_min = filter_var($_GET["filter"]["updated_at_min"], FILTER_SANITIZE_NUMBER_INT);

        $updated_date = date("Y-m-d H:i:s", strtotime('-2 hours', $updated_at_min));
        if (isset($_GET["io_salesorder_timezone_diff"]) && $_GET["io_salesorder_timezone_diff"] != "") {

            $io_salesorder_timezone_diff = filter_var($_GET["io_salesorder_timezone_diff"], FILTER_SANITIZE_NUMBER_INT);
            $updated_date = date("Y-m-d H:i:s", strtotime($io_salesorder_timezone_diff, $updated_at_min));
        }
        $prepared_args['date_query'][] = array('column' => 'post_modified_gmt', 'after' => $updated_date, 'inclusive' => true);
    }

    return $prepared_args;
}

add_action('save_post', 'add_novamodule_flag', 10, 3);

function add_novamodule_flag($post_id, $post, $update) {
    $slug = 'shop_order';

    //debugLogger("Checking nm_ns_pushed Meta for add_novamodule_flag");
    //debugLogger($slug);
    // If this isn't a 'woocommercer order' post, don't update it.
    if ($slug != $post->post_type) {
        return;
    }
    $order_id = $post->ID;

    //debugLogger(current_user_can('edit_shop_order', $order_id));

    /*if (!current_user_can('edit_' . $slug, $order_id)) {
        return;
    }*/
    $previousValue = get_post_meta($order_id, 'nm_ns_pushed', true);
    //debugLogger("Checking nm_ns_pushed Meta for " . $order_id . " Previous Value " . $previousValue);
    if ($previousValue != 1) {
        //debugLogger("Updating Post Meta for " . $order_id);
        update_post_meta($order_id, 'nm_ns_pushed', 0);
    }
}

//apply_filters( 'woocommerce_rest_prepare_customer', $response, $customer, $request )
add_filter("woocommerce_rest_prepare_customer", "novamodule_woocommerce_rest_prepare_customer_additional_meta", 10, 3);

function novamodule_woocommerce_rest_prepare_customer_additional_meta($response, $customer, $request) {
    //debugLogger("##############################");
    //debugLogger(json_encode($response));
    $extra_membership = get_option('extra_membership', '');
    if ($extra_membership != "" && isset($response->data["id"]) && $response->data["id"] > 0) {

        $customer_id = $response->data["id"];

        //$memberShipInformation = getCustomerMemberShipData($customer_id);
        //$response->data["customer_membership_post_data"] = $memberShipInformation;
        $memberShipInforData = getCustomeMemberShipInformation($customer_id);
        $response->data["customer_membership_data"] = $memberShipInforData;
    }
    return $response;
}

/* add_filter("woocommerce_rest_pre_insert_shop_order", "novamodule_woocommerce_rest_pre_insert_shop_order", 10, 2);

  function novamodule_woocommerce_rest_pre_insert_shop_order($order, $request ) {
  //debugLogger("############--order-############");
  //debugLogger(json_encode($request));
  //debugLogger(json_encode($request["customer_email"]));
  //debugLogger(json_encode($request["customer_id"]));
  if(isset($request["customer_email"]) && (!isset($request["customer_id"]) || $request["customer_id"] == 0 || $request["customer_id"] == "") ){
  $user = get_user_by( 'email', $request["customer_email"]);
  //debugLogger(json_encode($user));
  if($user && $user->ID > 0) {
  $request->customer_id  = $user->ID;
  }
  }
  //debugLogger(json_encode($request->customer_id));
  // debugLogger(json_encode($request));
  return $request;
  }

 */
//apply_filters( "woocommerce_rest_prepare_{$this->post_type}", $response, $post, $request )
add_filter("woocommerce_rest_prepare_shop_order", "novamodule_woocommerce_order_additional_meta", 10, 3);

function novamodule_woocommerce_order_additional_meta($response, $order, $request) {
    //debugLogger("##############################");
    //debugLogger(json_encode($response));
    //debugLogger(json_encode($order));
    // debugLogger($order->ID ."   hiiii");

    $extra_order_meta_keys = get_option('extra_order_meta_keys', '');
    $extra_order_meta_data = explode("\r\n", $extra_order_meta_keys);
    //debugLogger(json_encode($extra_order_meta_data));
    if (count($extra_order_meta_data) > 0) {
        foreach ($extra_order_meta_data as $newKey) {
            if (trim($newKey) != "") {
                $getMetaData = get_post_meta($order->ID, $newKey, true);
                $response->data[$newKey] = $getMetaData;
            }
        }
    }

    $extra_membership = get_option('extra_membership', '');

    if ($extra_membership != "" && isset($response->data["customer_id"]) && $response->data["customer_id"] > 0) {
        $customer_id = $response->data["customer_id"];
        //$memberShipInformation = getCustomerMemberShipData($customer_id);
        //$response->data["customer_membership_post_data"] = $memberShipInformation;

        $memberShipInforData = getCustomeMemberShipInformation($customer_id);
        $response->data["customer_membership_data"] = $memberShipInforData;
    }






    //debugLogger($getMetaData ."   hiiii");
    //debugLogger("##############################");

    return $response;
}

function getCustomerMemberShipData($customer_id) {
    $memberShipInformation = array();
    $lastposts = get_posts(array(
        'post_status' => "wcm-active", "post_type" => "wc_user_membership", "author" => $customer_id
    ));
    if (is_array($lastposts)) {

        foreach ($lastposts as $post) {
            $memberData = get_post($post->post_parent, 'ARRAY_A');
            if (count($memberData) > 0) {
                $memberShipInformation[] = $memberData;
            }
        }
    }
    return $memberShipInformation;
}

function getCustomeMemberShipInformation($user_id) {

    $memberShipInfo = array();
    $memberships = wc_memberships()->get_user_memberships_instance()->get_user_memberships($user_id);

    // count the memberships displayed
    $count = 0;

    if (!empty($memberships)) {

        foreach ($memberships as $membership) {

            $plan = $membership->get_plan();

            if ($plan && wc_memberships_is_user_active_member($user_id, $plan)) {

                $memberShipInfo[] = array("name" => $plan->name, "id" => $membership->id, "slug" => $plan->slug, "fullinfo" => $plan->post);
            }
        }
    }
    return $memberShipInfo;
}

################################END#############################################



add_action('user_register', 'nova_update_profile_last_update_time', 100);

function nova_update_profile_last_update_time($user_id) {

    update_user_meta($user_id, 'last_update', time());
}

function debugLogger($message) {
    $dir = dirname(__FILE__) . "/logs/novalogger.log";
    if (file_exists($dir)) {
        $file = fopen($dir, "a+");
        fwrite($file, $message);
        fwrite($file, "\n\n");
        fclose($file);
    }
}

######################REGISTER NEW META FIELDS ###############################

add_filter('admin_init', 'nova_general_settings_register_fields');

function nova_general_settings_register_fields() {
    register_setting('general', 'extra_order_meta_keys', 'esc_attr');
    add_settings_field('extra_order_meta_keys', '<label for="extra_order_meta_keys">' . __('Novamodule Additional Order Meta', 'extra_order_meta_keys') . '</label>', 'nova_general_settings_register_fields_html', 'general');

    register_setting('general', 'extra_membership', 'esc_attr');
    add_settings_field('extra_membership', '<label for="extra_membership">' . __('Novamodule Export Membership', 'extra_membership') . '</label>', 'nova_extra_membership_html', 'general');

    /* register_setting('general', 'extra_customer_meta_keys', 'esc_attr');
      add_settings_field('extra_customer_meta_keys', '<label for="extra_customer_meta_keys">' . __('Novamodule Additional Customer Meta', 'extra_customer_meta_keys') . '</label>', 'nova_general_settings_register_fields_customer_html', 'general'); */
}

function nova_general_settings_register_fields_html() {
    $value = get_option('extra_order_meta_keys', '');
    echo novagenerateField('extra_order_meta_keys', $value);
}

function nova_general_settings_register_fields_customer_html() {
    $value = get_option('extra_customer_meta_keys', '');
    echo novagenerateField('extra_customer_meta_keys', $value);
}

function nova_extra_membership_html() {
    $value = get_option('extra_membership', '');
    echo novagenerateFieldCheckBox('extra_membership', $value);
}

function novagenerateField($field, $value) {
    return '<textarea class="regular-text ltr"  id="' . $field . '" name="' . $field . '">' . esc_textarea($value) . "</textarea>";
}

function novagenerateFieldCheckBox($field, $value) {
    $checked = '';
    if ($value != "") {
        $checked = "checked = 'checked'";
    }
    return '<input type="checkbox" class="regular-text ltr"  id="' . $field . '" name="' . $field . '"  ' . $checked . '/>';
}

######################REGISTER NEW META FIELDS - END ###############################

