<?php
defined( 'ABSPATH' ) || exit;

//include_once("../Api/debugLogger.php");
include_once($_SERVER['DOCUMENT_ROOT']."/wp-content/plugins/novamodule-woocommerce/Api/debugLogger.php");
//include_once($_SERVER['DOCUMENT_ROOT']."/novamodule/wp-content/plugins/novamodule-woocommerce/Api/debugLogger.php");

class WC_REST_Bulkinventory_Controller  extends WC_REST_Controller {
	
	/**
	 * Endpoint namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'wc/v3';

	/**
	 * Route base.
	 *
	 * @var string
	 */
	protected $rest_base = 'bulkinventory';
    
	public $logger;

	/**
	 * Register routes.
	 *
	 * @since 3.5.0
	 */
	public function register_routes() {
		$this->logger = new NovaLogger("novamodule-bulkinventory.log");
		
		register_rest_route(
			$this->namespace, '/' . $this->rest_base,
				array(
					'methods' => WP_REST_Server::EDITABLE,
					'callback' => array( $this, 'edit_bulkinventory' ),
					'args' => $this->get_endpoint_args_for_item_schema( WP_REST_Server::EDITABLE ),
				)
		);
	}
	
	function __construct(){
		$this->logger = new NovaLogger("novamodule-bulkinventory.log");
		$this->logger->debugLogger("====================");
		//$this->register_routes();
		//parent::__construct();
	}
	
	public function edit_bulkinventory(WP_REST_Request $request) {

		$wp_rest_server = rest_get_server(); 
        $data = $request->get_params();

		$this->logger->debugLogger("========= edit_bulkinventory collection ===========");
		$this->logger->debugLogger(count($data));
		$this->logger->debugLogger(is_array($data));
		$this->logger->debugLogger(gettype($data));
		if (count($data) == count($data, COUNT_RECURSIVE)) 
		{
		  $response[] = array(
						'id'    => null,
						'error' => array(
							'code'    => 422,
							'message' => "Invalid Data format ".json_encode($data),
							'data'    => json_encode($data),
						),
					);
		 return $response;	
		}
		$this->logger->debugLogger("====================");
		
		for($i=0; isset($data[$i]) && $i<count($data); $i++) {
			
			$eachRow = $data[$i];
			$sku ='';
			$post_id = '';			
			if($eachRow['id']){
				$post_id = $eachRow['id'];
			}
			if($eachRow['sku']){
				$sku = $eachRow['sku'];
			}
			if(!$post_id && $sku) {
				$post_id = $this->getIdBySku($sku);
			} else if ($post_id && !$this->checkIfProductExists($post_id) && $sku){
				$post_id = $this->getIdBySku($sku);
			}
			if($post_id == '' || $post_id == null || !$post_id){
				$response[] = array(
					'id'    => null,
					'error' => array(
						'code'    => 422,
						'message' => "Invalid Data, either id or sku doesn't exists ".json_encode($eachRow),
						'data'    => json_encode($eachRow),
					),
				);
				continue;
			}
			
			$this->logger->debugLogger("=========edit_bulkinventory eachRow details ===========");
			$this->logger->debugLogger(json_encode($eachRow));
			$this->logger->debugLogger("====================");			
			
			$this->logger->debugLogger("=========edit_bulkinventory post_id details ===========");
			$this->logger->debugLogger(json_encode($post_id));
			$this->logger->debugLogger("====================");
				
			if($post_id && $post_id != '') {
				
				$product = wc_get_product((int)$post_id);
				$eachRow["id"] = (int)$post_id;
				if ('variation' === $product->get_type()) {
					if($product->get_parent_id() && !(isset($eachRow["product_id"]) && $eachRow["product_id"] != "" )){
						$eachRow["product_id"] = $product->get_parent_id();
					}
					if($eachRow["product_id"] == '' || $eachRow["product_id"] == null || !$eachRow["product_id"]){
						$response[] = array(
							'id'    => $eachRow["id"],
							'error' => array(
								'code'    => 422,
								'message' => "Invalid Data format, variant parent is missing ".json_encode($eachRow),
								'data'    => json_encode($eachRow),
							),
						);
						continue;
					}
					$_item = new WP_REST_Request( 'PUT' );
					$_item->set_body_params( $eachRow );
					$variations_controler = new WC_REST_Product_Variations_Controller;
					$_response = $variations_controler->update_item( $_item );
				} else {
					$_item = new WP_REST_Request( 'PUT' );
					$_item->set_body_params( $eachRow );
					$products_controller = new WC_REST_Products_Controller;
					$_response = $products_controller->update_item( $_item );					
				}
				if ( is_wp_error( $_response ) ) {
					$response[] = array(
						'id'    => $item['id'],
						'error' => array(
							'code'    => $_response->get_error_code(),
							'message' => $_response->get_error_message(),
							'data'    => $_response->get_error_data(),
						),
					);
				} else {
					$response[] = $wp_rest_server->response_to_data( $_response, '' );
				}
			}
		}
		return $response;
	}
	public function getIdBySku($sku) {
		global $wpdb;
		$post_id = '';
		$postmetatable = $wpdb->prefix."postmeta";
		$posttable = $wpdb->prefix."posts";
		$query = $wpdb->prepare("SELECT post_id from ".$postmetatable." as a INNER JOIN ".$posttable." as b ON a.post_id = b.id WHERE meta_key = '_sku' AND meta_value='%s' ORDER BY a.post_id DESC",$sku);		
		$results = $wpdb->get_results($query);
				
		foreach($results as $key =>$value){
			$post_id = $value->post_id;
			return $post_id;
		}

		return $post_id;
	}
	public function checkIfProductExists($post_ID = '') {
		if(!$post_ID){
			return false;
		}
		global $wpdb;
		
		$post_id = false;
		$posttable = $wpdb->prefix."posts";
		
		$post_ID = (int) $post_ID;
		
		$query = $wpdb->prepare( "SELECT ID FROM ".$posttable." WHERE post_type IN ('product','product_variation') AND ID =%d", $post_ID);
		
		$results = $wpdb->get_results( $query);

		foreach($results as $key =>$value){
			$post_id = $value->ID;
			return $post_id;
		}
		return $post_id;
	}
}
