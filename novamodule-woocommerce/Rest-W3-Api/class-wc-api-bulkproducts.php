<?php
defined( 'ABSPATH' ) || exit;

//include_once("../Api/debugLogger.php");
include_once($_SERVER['DOCUMENT_ROOT']."/wp-content/plugins/novamodule-woocommerce/Api/debugLogger.php");
//include_once($_SERVER['DOCUMENT_ROOT']."/novamodule/wp-content/plugins/novamodule-woocommerce/Api/debugLogger.php");

class WC_REST_Bulkproducts_Controller  extends WC_REST_Controller {
	
	/**
	 * Endpoint namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'wc/v3';

	/**
	 * Route base.
	 *
	 * @var string
	 */
	protected $rest_base = 'bulkproducts';
    
	public $logger;
	
	/**
	 * Register routes.
	 *
	 * @since 3.5.0
	 */
	public function register_routes() {
		$this->logger = new NovaLogger("novamodule-bulkproducts.log");
		$this->logger->debugLogger("====================");	
		register_rest_route(
			$this->namespace, '/' . $this->rest_base,
				array(
					'methods' => WP_REST_Server::EDITABLE,
					'callback' => array( $this, 'edit_bulkproducts' ),
					'args' => $this->get_endpoint_args_for_item_schema( WP_REST_Server::EDITABLE ),
				)
		);
	}
	
	function __construct(){
		$this->logger = new NovaLogger("novamodule-bulkproducts.log");
	}
	
	public function edit_bulkproducts(WP_REST_Request $request) {

		$wp_rest_server = rest_get_server(); 
        $data = $request->get_params();

		if (count($data) == count($data, COUNT_RECURSIVE)) 
		{
		  $response[] = array(
						'id'    => null,
						'error' => array(
							'code'    => 422,
							'message' => "Invalid Data format ".json_encode($data),
							'data'    => json_encode($data),
						),
					);
		 return $response;	
		}
		for($i=0; isset($data[$i]) && $i<count($data); $i++) {
			$eachRow = $data[$i];
			$sku ='';
			$post_id = '';			
			if($eachRow['id']){
				$post_id = $eachRow['id'];
			}
			if($eachRow['sku']){
				$sku = $eachRow['sku'];
			}
			if(!$post_id && $sku) {
				$post_id = $this->getIdBySku($sku);
			} else if ($post_id && !$this->checkIfProductExists($post_id) && $sku){
				$post_id = $this->getIdBySku($sku);
			}

			if(!($post_id || $sku)){
				$response[] = array(
					'id'    => null,
					'error' => array(
						'code'    => 422,
						'message' => "Invalid Data format, either id or sku doesn't exists ".json_encode($eachRow),
						'data'    => json_encode($eachRow),
					),
				);
				continue;
			}
			if($post_id && $post_id != '') {
				$product = wc_get_product((int)$post_id);
				$eachRow["id"] = (int)$post_id;				
				
				if ('variation' === $product->get_type()) {
					if($product->get_parent_id() && !(isset($eachRow["product_id"]) && $eachRow["product_id"] != "" )){
						$eachRow["product_id"] = $product->get_parent_id();
					}
					if($eachRow["product_id"] == '' || $eachRow["product_id"] == null || !$eachRow["product_id"]){
						$response[] = array(
							'id'    => $eachRow["id"],
							'error' => array(
								'code'    => 422,
								'message' => "Invalid Data format, variant parent is missing "."(".$eachRow["id"].")".json_encode($eachRow),
								'data'    => json_encode($eachRow),
							),
						);
						continue;
					}
					$parent = wc_get_product($eachRow['product_id']);
					if ( !$parent ) {
						$response[] = array(
							'id'    => $eachRow["id"],
							'error' => array(
								'code'    => 422,
								'message' => "Variation cannot be imported: Missing parent ID or parent does not exist yet."."(".$eachRow["id"].")".json_encode($eachRow),
								'data'    => json_encode($eachRow),
							),
						);
						continue;
					}
					if ( $parent->is_type( 'variation' ) ) {
						$response[] = array(
							'id'    => $eachRow["id"],
							'error' => array(
								'code'    => 422,
								'message' => "Variation cannot be imported: Parent product cannot be a product variation."."(".$eachRow["id"].")".json_encode($eachRow),
								'data'    => json_encode($eachRow),
							),
						);
						continue;
					}
					if ( $parent ) {
						if (isset($eachRow['attributes'])) {
							$eachRow['attributes'] = $this->reArrangeVariationAttributeData($parent, $eachRow['attributes']);
						}
						$this->logger->debugLogger("attributes final ".json_encode($eachRow['attributes']));
					}
					$_item = new WP_REST_Request( 'PUT' );
					$_item->set_body_params( $eachRow );
					$variations_controler = new WC_REST_Product_Variations_Controller;
					$_response = $variations_controler->update_item( $_item );
				} else {
					if('grouped' == $product->get_type()) {
						if(isset($eachRow['kit_line_item']) && $eachRow['kit_line_item']){
							$groupedItemIds = $this->getTheKitChildItemIds($eachRow['kit_line_item']);
							$eachRow['grouped_products'] = $groupedItemIds;
						}
					}
					if('variable' == $product->get_type()) {
						if (isset($eachRow['attributes'])) {
							$eachRow['attributes'] = $this->reArrangeParentAttributeData($eachRow['attributes']);
						}
						$this->logger->debugLogger("attributes final ".json_encode($eachRow['attributes']));
					}
					$_item = new WP_REST_Request( 'PUT' );
					$_item->set_body_params( $eachRow );
					$products_controller = new WC_REST_Products_Controller;
					$_response = $products_controller->update_item( $_item );
				}
				if ( is_wp_error( $_response ) ) {
					$response[] = array(
						'id'    => $item['id'],
						'error' => array(
							'code'    => $_response->get_error_code(),
							'message' => $_response->get_error_message(),
							'data'    => $_response->get_error_data(),
						),
					);
				} else {
					$response[] = $wp_rest_server->response_to_data( $_response, '' );
				}
			} else {
				if(isset($eachRow["type"]) && in_array($eachRow["type"], array('simple','grouped','external','variable','variation'))){
					if($eachRow["type"] == "variation") {
						if($sku != '' && (!$eachRow["product_id"] || $eachRow["product_id"] == "" || $eachRow["product_id"] == NULL) && isset($eachRow["variantParentSku"]) && $eachRow["variantParentSku"] != "") {
							$parentIdDetails = $this->getParentIdBySku($eachRow["variantParentSku"]);
							if(isset($parentIdDetails['sucess']) && $parentIdDetails['sucess'] == true && $parentIdDetails['post_id'] && $parentIdDetails['post_id'] != '') {
								$eachRow["product_id"] = $parentIdDetails['post_id'];
							} else if(isset($parentIdDetails['sucess']) && $parentIdDetails['sucess'] == false && $parentIdDetails['error'] && $parentIdDetails['error'] != '') {
								$response[] = array(
									'id'    => null,
									'error' => array(
										'code'    => 422,
										'message' => $parentIdDetails['error'],
										'data'    => json_encode($eachRow),
									),
								);
								continue;
							}
						}
						if (isset($eachRow["product_id"]) && $eachRow["product_id"] != "") {
							$parent = wc_get_product($eachRow['product_id']);
							if ( !$parent ) {
								$response[] = array(
									'id'    => null,
									'error' => array(
										'code'    => 422,
										'message' => "Variation cannot be imported: Missing parent ID or parent does not exist yet.".json_encode($eachRow),
										'data'    => json_encode($eachRow),
									),
								);
								continue;
							}
							if ( $parent->is_type( 'variation' ) ) {
								$response[] = array(
									'id'    => null,
									'error' => array(
										'code'    => 422,
										'message' => "Variation cannot be imported: Parent product cannot be a product variation.".json_encode($eachRow),
										'data'    => json_encode($eachRow),
									),
								);
								continue;
							}
							if ( $parent ) {
								if (isset($eachRow['attributes'])) {
									$eachRow['attributes'] = $this->reArrangeVariationAttributeData($parent, $eachRow['attributes']);
								}
								$this->logger->debugLogger("attributes final ".json_encode($eachRow['attributes']));
							}
							$_item = new WP_REST_Request( 'PUT' );
							$_item->set_body_params( $eachRow );
							$variations_controler = new WC_REST_Product_Variations_Controller;
							$_response = $variations_controler->create_item( $_item );
						} else {
							$response[] = array(
								'id'    => null,
								'error' => array(
									'code'    => 422,
									'message' => "Invalid Data format, variant parent is missing ".json_encode($eachRow),
									'data'    => json_encode($eachRow),
								),
							);
							continue;
						}
					} else {
						if('grouped' == $eachRow["type"]) {
							if(isset($eachRow['kit_line_item']) && $eachRow['kit_line_item']){
								$groupedItemIds = $this->getTheKitChildItemIds($eachRow['kit_line_item']);
								$eachRow['grouped_products'] = $groupedItemIds;
							}
						}
						if('variable' == $eachRow["type"]) {
							if (isset($eachRow['attributes'])) {
								$eachRow['attributes'] = $this->reArrangeParentAttributeData($eachRow['attributes']);
							}
							$this->logger->debugLogger("attributes final ".json_encode($eachRow['attributes']));
						}
						$_item = new WP_REST_Request( 'PUT' );
						$_item->set_body_params( $eachRow );
						$products_controller = new WC_REST_Products_Controller;
						$_response = $products_controller->create_item( $_item );
					}
				} else {
					$response[] = array(
						'id'    => null,
						'error' => array(
							'code'    => 422,
							'message' => "Invalid Data format, product type is wrong/missing ".json_encode($eachRow),
							'data'    => json_encode($eachRow),
						),
					);
					continue;
				}
				if ( is_wp_error( $_response ) ) {
					$response[] = array(
						'id'    => $item['id'],
						'error' => array(
							'code'    => $_response->get_error_code(),
							'message' => $_response->get_error_message(),
							'data'    => $_response->get_error_data(),
						),
					);
				} else {
					$response[] = $wp_rest_server->response_to_data( $_response, '' );
				}
			}
		
		}
		return $response;
	}
	public function getIdBySku($sku) {
		global $wpdb;
		$post_id = '';
		$postmetatable = $wpdb->prefix."postmeta";
		$posttable = $wpdb->prefix."posts";
		$query = $wpdb->prepare("SELECT post_id from ".$postmetatable." as a INNER JOIN ".$posttable." as b ON a.post_id = b.id WHERE meta_key = '_sku' AND meta_value='%s' ORDER BY a.post_id DESC",$sku);		
		$results = $wpdb->get_results($query);
		foreach($results as $key =>$value){
			$post_id = $value->post_id;
			return $post_id;
		}
		return $post_id;
	}
	public function checkIfProductExists($post_ID = '') {
		if(!$post_ID){
			return false;
		}
		global $wpdb;
		$post_id = false;
		$posttable = $wpdb->prefix."posts";
		$post_ID = (int) $post_ID;
		$query = $wpdb->prepare( "SELECT ID FROM ".$posttable." WHERE post_type IN ('product','product_variation') AND ID =%d", $post_ID);
		$results = $wpdb->get_results( $query);
		foreach($results as $key =>$value){
			$post_id = $value->ID;
			return $post_id;
		}
		return $post_id;
	}
	public function getParentIdBySku($sku) {
		global $wpdb;
		$post_id = '';
		$postmetatable = $wpdb->prefix."postmeta";
		$posttable = $wpdb->prefix."posts";
		$query = $wpdb->prepare("SELECT post_id from ".$postmetatable." as a INNER JOIN ".$posttable." as b ON a.post_id = b.id WHERE meta_key = '_sku' AND meta_value='%s' AND b.post_type IN ('product') ORDER BY a.post_id DESC", $sku);	
		$results = $wpdb->get_results($query);
		$result = array();
		$result["sucess"] = false;
		if(count($results) == 1) {
			$result["sucess"] = true;
			foreach($results as $key =>$value){
				$post_id = $value->post_id;
				$result["post_id"] = $post_id;
				return $result;
			}
		} elseif (count($results) > 1) {
			$message = "Multiple Items has same sku, ID List with same skus - ";
			foreach($results as $key =>$value){
				$post_id = $value->post_id;
				$message .= $post_id.", ";
			}
			$message = trim($message,',');
			$result["error"] = $message;
		}elseif (count($results) == 0)  {
			$message = "No item with sku ".$sku." exists in woo-commerce";
			$result["error"] = $message;
		}
		return $result;
	}
	/**
	 * Get variation parent attributes and set "is_variation".
	 *
	 * @param  array      $attributes Attributes list.
	 * @param  WC_Product $parent     Parent product data.
	 * @return array
	 */
	protected function get_variation_parent_attributes( $attributes, $parent ) {
		$parent_attributes = $parent->get_attributes();
		$require_save      = false;

		foreach ( $attributes as $attribute ) {
			$attribute_id = 0;

			// Get ID if is a global attribute.
			/*if ( ! empty( $attribute['taxonomy'] ) ) {
				$attribute_id = $this->get_attribute_taxonomy_id( $attribute['name'] );
			}*/
			$attribute_id = $this->get_attribute_taxonomy_id( $attribute['name'] );
			$this->logger->debugLogger('attribute_id  '.json_encode($attribute_id));
			if ( $attribute_id ) {
				$attribute_name = wc_attribute_taxonomy_name_by_id( $attribute_id );
			} else {
				$attribute_name = sanitize_title( $attribute['name'] );
			}
			$this->logger->debugLogger('attribute_name  '.json_encode($attribute_name));
			$this->logger->debugLogger('parent_attributes.attribute_name  '.json_encode($parent_attributes[ $attribute_name ]));
			// Check if attribute handle variations.
			if ( isset( $parent_attributes[ $attribute_name ] ) && ! $parent_attributes[ $attribute_name ]->get_variation() ) {
				// Re-create the attribute to CRUD save and generate again.
				$parent_attributes[ $attribute_name ] = clone $parent_attributes[ $attribute_name ];
				$parent_attributes[ $attribute_name ]->set_variation( 1 );
				$require_save = true;
			}
		}
		// Save variation attributes.
		if ( $require_save ) {
			$parent->set_attributes( array_values( $parent_attributes ) );
			$parent->save();
		}
		return $parent_attributes;
	}
	/**
	 * Get attribute taxonomy ID from the imported data.
	 * If does not exists register a new attribute.
	 *
	 * @param  string $raw_name Attribute name.
	 * @return int
	 * @throws Exception If taxonomy cannot be loaded.
	 */
	public function get_attribute_taxonomy_id( $raw_name ) {
		global $wpdb, $wc_product_attributes;
										
		// These are exported as labels, so convert the label to a name if possible first.
		$attribute_labels = wp_list_pluck( wc_get_attribute_taxonomies(), 'attribute_label', 'attribute_name' );
		$attribute_name   = array_search( $raw_name, $attribute_labels, true );

		if ( ! $attribute_name ) {
			$attribute_name = wc_sanitize_taxonomy_name( $raw_name );
		}

		$attribute_id = wc_attribute_taxonomy_id_by_name( $attribute_name );
		// Get the ID from the name.
		if ( $attribute_id ) {
			return $attribute_id;
		}
		// If the attribute does not exist, create it.
		$attribute_id = wc_create_attribute(
			array(
				'name'         => $raw_name,
				'slug'         => $attribute_name,
				'type'         => 'select',
				'order_by'     => 'menu_order',
				'has_archives' => false,
			)
		);
		if ( is_wp_error( $attribute_id ) ) {
			throw new Exception( $attribute_id->get_error_message(), 400 );
		}
		// Register as taxonomy while importing.
		$taxonomy_name = wc_attribute_taxonomy_name( $attribute_name );
		register_taxonomy(
			$taxonomy_name,
			apply_filters( 'woocommerce_taxonomy_objects_' . $taxonomy_name, array( 'product' ) ),
			apply_filters(
				'woocommerce_taxonomy_args_' . $taxonomy_name,
				array(
					'labels'       => array(
						'name' => $raw_name,
					),
					'hierarchical' => true,
					'show_ui'      => false,
					'query_var'    => true,
					'rewrite'      => false,
				)
			)
		);
		// Set product attributes global.
		$wc_product_attributes = array();
		foreach ( wc_get_attribute_taxonomies() as $taxonomy ) {
			$wc_product_attributes[ wc_attribute_taxonomy_name( $taxonomy->attribute_name ) ] = $taxonomy;
		}
		$this->logger->debugLogger('get_attribute_taxonomy_id '.json_encode($wc_product_attributes));
		return $attribute_id;
	}
	private function reArrangeVariationAttributeData($parent, $attributeInfo){
		$attributes = array();
		//$parent_attributes = $this->get_variation_parent_attributes($eachRow['attributes'], $parent );
		foreach ( $attributeInfo as $key => $attribute ) {
			if($attributeInfo[$key]["id"]) {
				continue;
			} else if($attributeInfo[$key]["name"]){
				$attribute_id = 0;
				$attribute_id = $this->get_attribute_taxonomy_id( $attribute['name'] );
				if ($attribute_id) {
					$attribute_name = wc_attribute_taxonomy_name_by_id( $attribute_id );
					$attributeInfo[$key]["id"] = $attribute_id;
				} else {
					$attribute_name = sanitize_title($attribute['name']);
				}
				$term_list = array();
				foreach( wc_get_product_terms( $parent->id, $attribute_name) as $attribute_value ){
					$this->logger->debugLogger('attribute_value '.$attribute_value->name);
					$term_list[] = $attribute_value->name;
				}
				$term_name = $attribute['option'];
				if( ! in_array( $term_name, $term_list)) {
					wp_set_post_terms( $parent->id, $term_name, $attribute_name, true );
				}
			}
		}
		return $attributeInfo;
	}
	
	private function reArrangeParentAttributeData($attributeInfo){
		$attributes = array();
		foreach ( $attributeInfo as $key => $attribute ) {
			if($attributeInfo[$key]["id"]) {
				continue;
			} else if($attributeInfo[$key]["name"]){
				$attribute_id = 0;
				$attribute_id = $this->get_attribute_taxonomy_id( $attribute['name'] );
				if ($attribute_id) {
					$attributeInfo[$key]["id"] = $attribute_id;
				}
			}
		}
		return $attributeInfo;
	}
	
	private function getTheKitChildItemIds($lineItems) {
		$groupItemIds = array();
		foreach($lineItems as $key => $value) {
			$sku = "";
			$productId = "";
			$sku = $key;
			if($value["wooitemid"]){
				if($this->checkIfProductExists($value["wooitemid"])){
					$productId = $value["wooitemid"];
				}
			}
			if(!$productId && $sku) {
				$productId = $this->getIdBySku($sku);
			}
			if($productId) {
				$groupItemIds[] = $productId;
			}
			$groupItemIds = array_unique($groupItemIds);
		}
		return $groupItemIds;
	}
}
